import { Component } from "react";
import { Link } from "react-router-dom";

class List extends Component {
    state = {
        players: []
    }

    componentDidMount() {
        fetch("http://localhost:3000/api/players")
            .then(res => res.json())
            .then(data => {
                this.setState({players: [...data.message]})
            })
    }

    renderPlayer = player => {
        return (
            <>
            <li key={player.username}>{player.username}</li>
            <li key={player.email}>{player.email}</li>
            <li key={player.experience}>{player.experience}</li>
            <li key={player.lvl}>{player.lvl}</li>            
            </>
        )
    }

    render() {
        return (
            <>
                <Link to="/form">Create new player</Link>
                <ul>
                    {this.state.players.map(this.renderPlayer)}
                </ul>
            </>
        )
    }
}

export default List